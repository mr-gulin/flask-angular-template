# Flask Angular Template

## Init new project

1. Форкнуть этот репозиторий
2. Создать runner в gitlab:
    1. `sudo gitlab-runner register`
    2. Указать `https://gitlab.com` в качестве хоста
    3. Указать токен с текущего проекта (project -> settings -> CI/CD -> Runners)
    4. Указать имя раннера. Темплейт `$PROJECT_NAME-runner` (имя проекта как название репозитория)
    5. Теги - не нужно.
    6. Executor - `shell`
3. Добавить в раннер volume
    1. `sudo nano /etc/gitlab-runner/config.toml`
    2. Добавить в секцию volumes как элемент массива: `"/home/andrew/projects:/projects:rw"`
4. Отключить Shared Runners (project -> settings -> CI/CD -> Runners)
5. Запустить раннер `sudo gitlab-runner start $runner-name`
6. Разрешить раннеру запускать jobs без тегов (project -> settings -> CI/CD -> Runners)
7. Добавить переменные для раннера в проект:
    1. `$PROJECT_PATH` - путь до проекта на хосте относительно `/home/andrew/projects/` eg: `flask-angular-template`
8. Создать ресурсную запись домена eg `test.andrewgulin.ru`
9. Добавить сервер в nginx, указывающий на папку со статикой `/home/andrew/projects/project/static/`

### БД и .env
1. Сделать сложный пароль
2. Войти в mysql под root и создать нового пользователя и новую базу данных
    * `CREATE USER '$DB_USER'@'localhost' identified by '$DB_PASSWORD';`
    * `CREATE DATABASE $DB_NAME;`
    * `GRANT ALL PRIVILEGES ON $DB_NAME.* TO 'USERNAME'@'localhost';`
    * `FLUSH PRIVILEGES;`
3. В папке проекта создать .env файл с переменными: 
    * `SQLALCHEMY_DATABASE_URI='mysql+pymysql://$DB_USER:$DB_PASSWORD@localhost:3306/$DB_NAME'`
    * `SQLALCHEMY_TRACK_MODIFICATIONS=False`
    * `FLASK_ENV=production`
4. В папке проекта инициализировать миграции `flask db init`
    
### Сервис
1. Настроить `app.ini` (прописать имя сокета)
2. Создать сервис `sudo nano /etc/systemd/system/$PROJECT_NAME.service`
3. Прописать:
``` 
[Unit]
Description=uWSGI instance to serve myproject
After=network.target

[Service]
User=sammy
Group=www-data
WorkingDirectory=/home/andrew/projects/myproject
Environment="PATH=/home/andrew/projects/myproject/.venv/bin"
ExecStart=/home/andrew/projects/myproject/.venv/bin/uwsgi --ini app.ini

[Install]
WantedBy=multi-user.target
```

4. Cконфигурировать nginx (сокет)
```
server {
    listen 80;
    server_name your_domain www.your_domain;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/home/andrew/projects/myproject/myproject.sock;
    }
}
```
(Добавить в уже существующую конфигурацию)


