from app import app
from flask import jsonify

@app.route('/')
@app.route('/index')
@app.route('/api')
def index():
    return jsonify({'test': 'test2'})
