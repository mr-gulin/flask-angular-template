import {Component, OnInit} from '@angular/core';
import {HttpService} from './services/http.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
    title = 'annie-art';
    data = {};

    constructor(private httpService: HttpService) {
    }

    ngOnInit(): void {
        this.httpService.getTestData().subscribe((data: any) => {
            this.data = data;
            console.log(data);
        });
    }

}
