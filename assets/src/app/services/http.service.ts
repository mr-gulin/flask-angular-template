import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class HttpService {
    private baseUrl = environment.baseUrl;

    constructor(private http: HttpClient) { }


    getTestData() {
        return this.http.get(this.baseUrl);
    }
}
